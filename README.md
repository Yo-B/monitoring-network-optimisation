# Monitoring Network Optimisation

A machine learning approach to monitoring network optimisation.


## Libraries

[Python](https://www.python.org/) 3.6+, [Tensorflow](https://www.tensorflow.org/) 2.2+ and [Keras](https://keras.io/) 2.3+ are needed to run the script.


## Data

The dataset needed to train the network and produce UK-wide predictions is available at [Rothamsted repository](https://repository.rothamsted.ac.uk/item/979yv/aphid-arrival-in-the-uk-from-1965-to-2018-measured-by-the-ris-suction-trap-network). The code include download commands.


## Publication

This script support a scientific paper published in [Environemental Modelling and Software](https://www.sciencedirect.com/science/article/pii/S1364815220309828).