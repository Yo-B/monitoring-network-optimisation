import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Activation, Input, Dropout, add, Lambda, BatchNormalization
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
from sklearn.ensemble import BaggingRegressor
from sklearn.preprocessing import StandardScaler
import seaborn as sns
from plotnine import *

## Meta PARAMETERS
## quantiles to estimate for the quantile regression
quantiles = np.array([0.05, 0.5, 0.95])
## adapt the following to your memory capacity
n_regressors = 30 # Number of models to be bagged (~30 is good but memory demanding)
l_size_shared = 128 # shared network, main layers size
l_size_species = 32 # species specific layers size
l_size_prior = 16 # untrained network layer size

# ## LOAD DATA from local files
# training = pd.read_csv("data/aphid_training_dataset.csv") # D1D measurements in traps 1965-2018 + covariates
# grid = pd.read_csv("data/prediction_grid_dataset.csv") # covariates on UK grid 2015-2018, for predictions

## or LOAD DATA from urls dataset
url = "https://repository.rothamsted.ac.uk/download/7baeab7dc70eab561b518439ff11e4ae268e0eacd38dce07c43398350f15b3f8/474911/aphid_training_dataset.csv"
training = pd.read_csv(url) # D1D measurements in traps 1965-2018 + covariates
url = "https://repository.rothamsted.ac.uk/download/3ca6319e289ffe8c754238ce5b586318c25077fe4804345e70ec429ef01611db/18500209/prediction_grid_dataset.csv"
grid = pd.read_csv(url) # covariates on UK grid 2015-2018, for predictions

## merge the datasets, add a column factor to differentiate them
bigdata = pd.merge(training, grid, how='outer')
bigdata['what'] = np.concatenate([['resp']*training.shape[0], ['grid']*grid.shape[0]])
vars = np.array(["coord"]*5 + ["sp"]*14 + ["var"]*(bigdata.shape[1]-14-6) + ["NA"])

## aphid code names, for more informative plot titles
aphidName = pd.DataFrame({"sp": ["sp318","sp114","sp290","sp91","sp376","sp420","sp243",
                            "sp292","sp355","sp358","sp396","sp322","sp112","sp110",
                            "sp308","sp111","sp410","sp132","sp389","sp421","sp264"],
                        "name": ["M. ascalonicus","R. padi","E. abietinum","D. platanoidis",
                        "A. solani","S. avenae","B. helichrysi","C. aegopodii","N. ribisnigri",
                        "H. lactucae","M. dirhodum","M. persicae","R. maidis","H. pruni",
                        "P. humuli","R. oxyacanthae","M. euphorbiae","A. fabae","A. pisum",
                        "S. fragariae","B. brassicae"]})

## select training/prediction datasets
x_data = bigdata.loc[bigdata.what == "resp", vars == "var"].reset_index(drop=True)
y_data = bigdata.loc[bigdata.what == "resp", vars == "sp"].reset_index(drop=True)
cov_data = bigdata.loc[bigdata.what == "grid", vars == "var"].reset_index(drop=True)
trap_locs = bigdata.loc[bigdata.what == "resp", ].iloc[:, 0:3].reset_index(drop=True)
coords = bigdata.loc[bigdata.what == "grid", ].iloc[:, 0:3].reset_index(drop=True)
aphids = bigdata.columns[vars == "sp"] ## focus on fewer species is possible here

## remove some useless variables
to_rm = ["CORINE_lvl1_main", "CORINE_lvl2_main", "CORINE_lvl3_main"]
x_data = x_data.drop(to_rm, 1)
cov_data = cov_data.drop(to_rm, 1)

## add buffer distances
locs = trap_locs.drop(["year"], 1).drop_duplicates()
pts = locs.append(pd.DataFrame(
    data=coords.drop(["year"], 1).drop_duplicates()))
for i in range(locs.shape[0]):
    dist = np.sqrt((locs.x.iloc[i] - pts.x)**2 + (locs.y.iloc[i] - pts.y)**2)
    pts = pd.concat([pts, dist], axis=1)
pts.columns = ["x", "y"] + ["d2trap" + str(i + 1) for i in range(locs.shape[0])]
x_data = pd.concat([x_data.reset_index(drop=True),
                    pd.DataFrame.merge(trap_locs, pts, on=["x", "y"], how="left").drop(["year", "x", "y"], 1)], axis=1)
cov_data = pd.concat([cov_data.reset_index(drop=True),
                      pd.DataFrame.merge(coords, pts, on=["x", "y"], how="left").drop(["year", "x", "y"], 1)], axis=1)

## select correlated variables to remove
corr = x_data.corr()
to_rm = []
max_corr = 1
while max_corr > .95:
    mat = x_data.drop(to_rm, 1)
    to_rm = to_rm + [mat.columns[np.unravel_index(
        np.triu(mat.corr().abs(), k=1).argmax(axis=None), mat.corr().shape)[1]]]
    max_corr = np.triu(mat.corr().abs(), k=1).max()
x_data = x_data.drop(to_rm, 1)
cov_data = cov_data.drop(to_rm, 1)

## select training years
years = list(range(1980, 2018 + 1))
who_train = np.array((trap_locs.year >= min(years)) & (trap_locs.year <= max(years)))
x_data = x_data.loc[who_train, ]
y_data = y_data.loc[who_train, ]
trap_locs = trap_locs.loc[who_train, ]

## select one or more aphid species as response variables
resp = aphids
aphidName = aphidName.iloc[[np.min(np.where(aphidName.sp == r)) for r in resp],]
y_data = np.array(y_data[resp]).reshape(y_data.shape[0], len(resp))
x_data = np.array(x_data)
trap_locs = np.array(trap_locs)

## remove lines full of NaNs
keep = np.sum(np.isnan(y_data), axis=1) < 1 ## bad idea to change one for e.g. 5
trap_locs = trap_locs[keep, ]
x_data = x_data[keep, ]
y_data = y_data[keep, ]

## scaling
scaler_x = StandardScaler()
scaler_y = StandardScaler()
## fit scaler on training dataset
scaler_x.fit(np.concatenate((x_data, cov_data)))
scaler_y.fit(y_data)
## transform training dataset
y_data = scaler_y.transform(y_data)
x_data = scaler_x.transform(x_data)

## custom loss function
def pinball_loss(y_true, y_pred, tau):
    e = K.tile(y_true, (1, K.int_shape(tau)[0])) - y_pred
    return K.mean(K.maximum(tau*e, (tau-1)*e), axis=-1)
loss_pb = lambda y_true, y_pred: pinball_loss(y_true, y_pred, quantiles)

## function to get a randomized prior functions model
def get_quantile_reg_rpf_nn(input_dim, q_dim, sp_dim):
    ## shared input of the network
    net_input = Input(shape=(input_dim,), name='input')
    ## trainable network body
    trainable_net = Sequential(
                        [Dense(l_size_shared, use_bias=False, kernel_initializer='he_uniform'),
                        BatchNormalization(), Activation('relu'), Dropout(0.1),
                        Dense(l_size_shared, use_bias=False, kernel_initializer='he_uniform'),
                        BatchNormalization(), Activation('relu'), Dropout(0.1),
                        Dense(l_size_shared, use_bias=False, kernel_initializer='he_uniform'),
                        BatchNormalization(), Activation('relu'), Dropout(0.1)],
                        name='trainable_shared')(net_input)
    ## trainable multiouputs
    train_out = [None] * sp_dim
    for i in range(sp_dim):
        train_out[i] = Sequential([
                        Dense(l_size_species, activation='tanh', kernel_initializer='glorot_uniform'),
                        Dense(q_dim, activation='linear', kernel_initializer='glorot_uniform')],
                        name="train_out_"+str(i))(trainable_net)
    ## prior network body
    prior_net  = Sequential([Dense(l_size_prior, activation='elu', kernel_initializer='glorot_normal', trainable=False),
                            Dense(l_size_prior, activation='elu', kernel_initializer='glorot_normal', trainable=False),
                            Dense(l_size_prior, activation='elu', kernel_initializer='glorot_normal', trainable=False)],
                           name='prior_shared')(net_input)
    ## prior network outputs
    prior_out = [None] * sp_dim
    for i in range(sp_dim):
        prior_out[i] = Dense(q_dim, activation='linear', kernel_initializer='glorot_normal',
                            trainable=False, name="prior_out_"+str(i))(prior_net)
        # # using a lambda layer so we can control the weight (beta) of the prior network
        # prior_out[i] = Lambda(lambda x: x * 1, name="prior_scale_"+str(i))(prior_out[i])
    ## adding all the outputs together
    net_outputs = [None] * sp_dim
    for i in range(sp_dim):
        net_outputs[i] = add([train_out[i], prior_out[i]], name='add_out_'+str(i))
    ## build the model
    model = Model(net_input, net_outputs)
    model.compile(loss=[loss_pb]*sp_dim, experimental_run_tf_function=False,
                  optimizer=tf.keras.optimizers.Adam(learning_rate=0.001))
    ## returning the model
    return model

# ## plot the model network
# tf.keras.utils.plot_model(
#         get_quantile_reg_rpf_nn(input_dim=x_data.shape[1], q_dim=len(quantiles), sp_dim=2),
#         to_file='figures/model_rpf.png', show_shapes=False, show_layer_names=True,
#         rankdir='TB', expand_nested=False, dpi=96)


## ---------------
##  Single NETWORK
## ---------------
# trained = np.isin(np.arange(x_data.shape[0]),
#                   np.random.choice(x_data.shape[0],
#                                    int(np.round(x_data.shape[0] * (1 - .2))), replace=False))
#
# model = get_quantile_reg_rpf_nn(input_dim=x_data.shape[1], q_dim=len(quantiles), sp_dim=len(resp))
#
# fit_hist = model.fit(x_data[trained,:], [y_data[trained,i] for i in range(y_data.shape[1])],
#                     validation_data=[x_data[~trained,:], [y_data[~trained,i] for i in range(y_data.shape[1])]],
#                     callbacks=[EarlyStopping(monitor='val_loss', patience=50, restore_best_weights=True)],
#                     epochs=3000, batch_size=10, verbose=0, shuffle=True)
# ## training curve
# plt.figure(figsize=(10, 7))
# plt.plot(fit_hist.history['loss'])
# plt.plot(fit_hist.history['val_loss'])
# plt.show()
# ## residuals
# preds = model.predict(x_data)
# quantile_output = np.empty(shape=(y_data.shape[0], y_data.shape[1], 3))
# for i in range(len(resp)):
#     quantile_output[:,i,:] = preds[i]
# p05_median = scaler_y.inverse_transform(quantile_output[:,:,0])
# p50_median = scaler_y.inverse_transform(quantile_output[:,:,1])
# p95_median = scaler_y.inverse_transform(quantile_output[:,:,2])
# obs = scaler_y.inverse_transform(y_data)
# (np.sqrt(np.mean((p50_median[trained,:] - obs[trained,:])**2)),
#  np.sqrt(np.mean((p50_median[~trained,:] - obs[~trained,:])**2)))
#
# df = pd.DataFrame({'obs': obs.ravel(), 'pred': p50_median.ravel(),
#             'bot': p05_median.ravel(), 'top': p95_median.ravel(),
#             'trained': np.repeat(trained, obs.shape[1]), 'sp': np.tile(resp, obs.shape[0])})
# ggplot(df) + facet_wrap('~sp', nrow=3) + geom_abline(slope=1) + theme_minimal() +\
#     geom_errorbar(aes(x='obs', ymin='bot', ymax='top', colour='trained'), alpha=.3, size=.3) +\
#     geom_point(aes(x='obs', y='pred', colour='trained'), size=.1)

## ---------------
##  Prepare for BAGGING
## ---------------

class MyMultiOutputKerasRegressor(KerasRegressor):
    # initialisation
    def __init__(self, **kwargs):
        KerasRegressor.__init__(self, **kwargs)
    # custom fit method
    def fit(self, X, y, **kwargs):
        KerasRegressor.fit(self, X, [y[:,i] for i in range(y.shape[1])],
                        callbacks=[EarlyStopping(monitor='val_loss', patience=50, restore_best_weights=True)],
                        validation_split=.1, shuffle=True, **kwargs)

# wrapping our base model around a sklearn estimator
base_model = MyMultiOutputKerasRegressor(
            build_fn=get_quantile_reg_rpf_nn,
            input_dim=x_data.shape[1], q_dim=len(quantiles), sp_dim=len(resp),
                                         epochs=2500, batch_size=10, verbose=0)

## ---------------
##  BAGGING
## ---------------

# create a bagged ensemble of 10 base models
bag = BaggingRegressor(base_estimator=base_model, n_estimators=n_regressors, verbose=2, n_jobs=1)

# still a train/test cut is needed to check performance
test_p = .3 ## proportion of samples to reserve in testing set
trained = np.isin(np.arange(x_data.shape[0]), np.random.choice(x_data.shape[0],
                                   int(np.round(x_data.shape[0] * (1 - test_p))), replace=False))
# fitting the ensemble
bag.fit(x_data[trained,:], y_data[trained,:])


# ## ---------------
# ##  Save the models (only weights, because of the custom loss function we cant save models)
# ## ---------------
# from datetime import datetime
# import os
# dir_name = "models/model_"+datetime.now().strftime("%d%b%Y_%H%M")
# os.mkdir(dir_name)
#
# ## save model list
# for i in range(len(bag.estimators_)):
#     bag.estimators_[i].model.save_weights(dir_name+"/model_weights_"+str(i))
#
# ## load model list
# model_list = list()
# for i in range(int((len(os.listdir(dir_name))-1)/2)):
#     model = get_quantile_reg_rpf_nn(input_dim=x_data.shape[1], q_dim=len(quantiles), sp_dim=len(resp))
#     model.load_weights(dir_name+"/model_weights_"+str(i))
#     model_list.append(model)


## --------------------
##  PLOTING residuals
## --------------------

## plot bagged residuals
quantile_output = np.empty(shape=(len(bag.estimators_), y_data.shape[0], y_data.shape[1], 3))
for j in range(len(bag.estimators_)):
    preds = bag.estimators_[j].predict(x_data)
    for i in range(len(resp)):
        quantile_output[j,:,i,:] = preds[i]

p05_median = scaler_y.inverse_transform(np.quantile(quantile_output[:,:,:,0], 0.5, axis=0))
p50_median = scaler_y.inverse_transform(np.quantile(quantile_output[:,:,:,1], 0.5, axis=0))
p95_median = scaler_y.inverse_transform(np.quantile(quantile_output[:,:,:,2], 0.5, axis=0))
obs = scaler_y.inverse_transform(y_data)

pred = [p05_median, p50_median, p95_median ]
res = pd.DataFrame({"species": np.tile(aphidName.name.ravel(), p05_median.shape[0]),
                    "q05": p05_median.transpose().ravel(),
                    "q50": p50_median.transpose().ravel(),
                    "q95": p95_median.transpose().ravel(),
                    "obs": scaler_y.inverse_transform(y_data).transpose().ravel(),
                    "trained": pd.Categorical(np.tile(trained, p05_median.shape[1]), categories=[True, False])})

plot = ggplot(res) + geom_abline(slope=1) +\
            geom_segment(aes(x="obs", xend="obs", y="q05", yend="q95", colour="trained"), size=.1) +\
            geom_point(aes(x="obs", y="q50", colour="trained"), size=.1) +\
            facet_wrap("~species") + theme_minimal() + labs(x="Observed D1D", y="Predicted D1D") +\
            scale_color_manual(name="Trained", values=("#1f77b4", "#ff7f0e")) +\
            theme(legend_position=(.7, .15), legend_direction="horizontal",
              strip_text_x=element_text(style="italic"))
plot
# plot.save("figures/residuals.png", width=10, height=7, dpi=300)

## quantile consistency
(np.mean((p05_median < p50_median) & (p50_median < p95_median)),
    np.mean((p05_median < obs) & (obs < p95_median)))

## RMSE per species
pred = [p05_median, p50_median, p95_median ]
rmse = pd.DataFrame(columns=["species", "what", "q", "RMSE"])
rmse = rmse.astype(
    dtype={"species": str, "what": str, "q": float, "RMSE": float})
for i in range(len(resp)):
    for j in range(len(quantiles)):
        temp = pd.DataFrame(np.c_[resp[[i, i, i]], ["total", "train", "test"], quantiles[[j, j, j]],
                        [np.sqrt(np.mean((pred[j][:,i] - obs[:,i])**2)),
                        np.sqrt(np.mean((pred[j][trained,i] - obs[trained,i])**2)),
                        np.sqrt(np.mean((pred[j][~trained,i] - obs[~trained,i])**2))]],
                 columns=["species", "what", "q", "RMSE"])
        temp = temp.astype(dtype={"species": str, "what": str, "q": float, "RMSE": float})
        rmse = rmse.append(temp)
rmse.groupby(['what', "q"])['RMSE'].mean()
rmse[(rmse.q==0.5) & (rmse.what=="test")].groupby(['what', "species"])['RMSE'].mean()

## barplot of RMSE per species
temp = rmse[(rmse.q==0.5)].groupby(['what', "species"])['RMSE'].mean().reset_index()
temp = pd.DataFrame.merge(temp, aphidName, left_on='species', right_on='sp').drop(columns=['sp'])

plot = (ggplot(data=temp) + geom_col(aes(x="species", y="RMSE", fill="what"),
                    stat='identity', position='dodge') + \
    scale_fill_brewer(type='qual', palette=6) + scale_colour_brewer(type='qual', palette=6) +\
    theme_bw() + theme(axis_text_x=element_text(angle=45)) + labs(x="") +\
    geom_hline(aes(yintercept="RMSE", colour="what"),
        data=rmse[(rmse.q==0.5)].groupby(['what'])['RMSE'].mean().reset_index()) +\
    geom_text(aes(x='species', y=50, label='name'), angle=90, va='top', parse=True))
plot
# plot.save("figures/rmse_hist.png", width=10, height=7, dpi=300)



## ----------------------------
##  PREDICTIONS on UK Grid
## ----------------------------

## select years (subsequently averaged)
pred_years = range(2015, 2018+1)
who = np.array((coords.year >= min(pred_years)) & (coords.year <= max(pred_years)))
coords_sc = coords.loc[who, ]
cov_data_sc = scaler_x.transform(cov_data.loc[who, ])

## plot bag pred-obs
model_list = bag.estimators_
quantile_output = np.empty(shape=(len(model_list), cov_data_sc.shape[0], y_data.shape[1], 3))
for j in range(len(model_list)):
    preds = model_list[j].predict(cov_data_sc)
    print(".", end="")
    for i in range(len(resp)):
        quantile_output[j,:,i,:] = preds[i]

## make data.DataFrame
temp_coords = pd.concat([coords_sc]*len(quantiles)*3).reset_index(drop=True)
temp_opt = pd.DataFrame({"what": np.repeat(["mean", "se", "q_pred"], coords_sc.shape[0]*len(quantiles)),
                         "q": np.tile(np.repeat(quantiles, coords_sc.shape[0]), 3)})
temp_sp = pd.DataFrame(
    scaler_y.inverse_transform(
        np.vstack([np.mean(quantile_output[:,:,:,0], axis=0),
            np.mean(quantile_output[:,:,:,1], axis=0),
            np.mean(quantile_output[:,:,:,2], axis=0),
            np.sqrt(np.var(quantile_output[:,:,:,0], axis=0)) / np.sqrt(len(quantiles)),
            np.sqrt(np.var(quantile_output[:,:,:,1], axis=0)) / np.sqrt(len(quantiles)),
            np.sqrt(np.var(quantile_output[:,:,:,2], axis=0)) / np.sqrt(len(quantiles)),
            np.quantile(quantile_output[:,:,:,0], 0.05, axis=0),
            np.quantile(quantile_output[:,:,:,1], 0.5, axis=0),
            np.quantile(quantile_output[:,:,:,2], 0.95, axis=0)])),
    columns=resp).reset_index(drop=True)
df = pd.concat([temp_coords, temp_opt, temp_sp], axis=1, sort=False)

## write the dataframe of predictions
# df.to_csv("outputs/exportMap.csv", index=False)


## --------------------------------------------------------
## Plot per-species Uncertainty distribution across the UK
## --------------------------------------------------------

## (1) epistemic uncertainty of the lower quantile
preds_E = df[(df.q == 0.05) & (df.what == 'se')].groupby(['x', 'y']).mean().reset_index()
preds_long_E = pd.melt(preds_E, id_vars=df.columns[0:3], value_vars=preds_E.columns[4:], var_name="sp")
preds_long_E = pd.merge(preds_long_E, aphidName[['sp', 'name']])
## add avergae crops, and reorder
avg = preds_long_E.groupby(['x', 'y'])['value'].mean().reset_index()
avg['sp'] = 'avg'
avg['name'] = 'Average'
temp = preds_long_E.append(avg)
temp['name'] = pd.Categorical(temp.name, \
    categories=['A. fabae', 'B. brassicae', 'B. helichrysi',
       'C. aegopodii', 'D. platanoidis', 'E. abietinum', 'H. pruni',
       'M. ascalonicus', 'M. persicae', 'N. ribisnigri', 'P. humuli',
       'R. maidis', 'R. oxyacanthae', 'R. padi', 'Average'])

plot = ggplot(temp) + geom_tile(aes(x='x', y='y', fill='value')) + \
    facet_wrap('~name', nrow=2) + coord_equal() + theme_void() + \
    scale_fill_gradientn(name=r'$\frac{sd(q_{0.05})}{\sqrt{n}}$',
                        colors=sns.color_palette('Spectral_r', 100)) +\
    theme(strip_text_x=element_text(size=10, margin={'t':0, 'b':-40, 'l':0, 'r':0}, hjust=.5),
       legend_position=(.87,.31), legend_title=element_text(size=14, hjust=-.5),
       panel_spacing_x=-.25, panel_spacing_y=.15,
       legend_text_colorbar=element_text(size=12)) +\
    guides(fill= guide_colorbar(barheight=150))
plot
# plot.save(filename='map_EpisUncert_rpf.png', width = 15, height = 8, dpi = 300)

## (2) aleatory uncertainty: q95-q05
q05 = df[(df.q == 0.05) & (df.what == 'mean')].groupby(['x', 'y']).mean().reset_index()
q95 = df[(df.q == 0.95) & (df.what == 'mean')].groupby(['x', 'y']).mean().reset_index()
q95.iloc[:,4:] = q95.iloc[:,4:] - q05.iloc[:,4:]
preds_A = q95
preds_long_A = pd.melt(preds_A, id_vars=df.columns[0:3], value_vars=q95.columns[4:], var_name="sp")
preds_long_A = pd.merge(preds_long_A, aphidName[['sp', 'name']])
## add avergae crops, and reorder
avg = preds_long_A.groupby(['x', 'y'])['value'].mean().reset_index()
avg['sp'] = 'avg'
avg['name'] = 'Average'
temp = preds_long_A.append(avg)
temp['name'] = pd.Categorical(temp.name, \
    categories=['A. fabae', 'B. brassicae', 'B. helichrysi',
       'C. aegopodii', 'D. platanoidis', 'E. abietinum', 'H. pruni',
       'M. ascalonicus', 'M. persicae', 'N. ribisnigri', 'P. humuli',
       'R. maidis', 'R. oxyacanthae', 'R. padi', 'Average'])

plot = ggplot(temp) + geom_tile(aes(x='x', y='y', fill='value')) + \
    facet_wrap('~name', nrow=2) + coord_equal() + theme_void() + \
    scale_fill_gradientn(name=r'$\overline{q_{0.95}}-\overline{q_{0.05}}$',
                        colors=sns.color_palette('Spectral_r', 100)) +\
    theme(strip_text_x=element_text(size=10, margin={'t':0, 'b':-40, 'l':0, 'r':0}, hjust=.5),
       legend_position=(.87,.31), legend_title=element_text(size=14, hjust=-.5),
       panel_spacing_x=-.25, panel_spacing_y=.15,
       legend_text_colorbar=element_text(size=12)) +\
    guides(fill= guide_colorbar(barheight=150))
plot
# plot.save(filename='map_AleaUncert_rpf.png', width = 15, height = 8, dpi = 300)



## ----------------------
## Next trap location?
## ----------------------

## as the practical objective requires the distribution of the specific Crops
## which is distributed under restricted use: https://www.ceh.ac.uk/crops2015
## we only provide here our answer to the epistemic objective which does not
## need such additional data

## traps locations
temp = bigdata[bigdata.what=='resp']
now = temp[['x', 'y']][temp.year==2018]
old = temp[['x', 'y']][temp.year<2018].drop_duplicates()
old = old.loc[~np.array([i in np.array(now.x) for i in np.array(old.x)]),:]
old['Traps'] = 'past'
now['Traps'] = 'current'
traps = old.append(now)

## average epistemic uncertainty and use proportion of crops to mask non-agricol areas
temp = pd.merge(preds_E, bigdata[["x", "y", "CORINE_lvl1_2"]].drop_duplicates())
temp_long = pd.melt(temp, id_vars=temp.columns[[0,1,2,18]], value_vars=temp.columns[4:18], var_name="sp")
temp_long = pd.merge(temp_long, aphidName[['sp', 'name']])
temp_long = temp_long.groupby(['x', 'y']).mean().reset_index()
temp_long.loc[temp_long.CORINE_lvl1_2 < .33, 'value'] = np.NaN

plot_A = ggplot(temp_long) + geom_tile(aes(x='x', y='y', fill='value')) + \
    coord_equal() + theme_minimal() + \
    scale_fill_gradientn(name=r'$V^E_{\mathbf{x}}$', \
                        colors=sns.color_palette('Spectral_r', 100),
                        limits=[np.min(temp_long.value), np.quantile(temp_long.value[~np.isnan(temp_long.value)], q=.995)]) +\
    geom_tile(aes(x='x', y='y'), fill='lightgrey', data=temp_long.loc[np.isnan(temp_long.value)]) + \
    geom_point(aes(x='x', y='y', colour='Traps', size='Traps', shape='Traps'), fill='red', data=traps) +\
    scale_color_manual(values=['black', 'red']) + scale_size_manual(values=[2, 2]) +\
    scale_shape_manual(values=['o', 'x']) +\
    theme(legend_position=(.8,.6), legend_text=element_text(size=12), legend_title=element_text(size=13),
        strip_text_x=element_text(size=14, margin={'t':0, 'b':-90, 'l':0, 'r':0}, hjust=.5),
       panel_spacing_x=-.25, axis_text=element_blank(), axis_title=element_blank())
plot_A
